/*******************************************************************************
 * Copyright 卫志强 QQ：598748873@qq.com Inc. All rights reserved. 开源地址：https://gitee.com/doc_wei01/skyeye
 ******************************************************************************/

package com.skyeye.common.constans;

/**
 * @ClassName: Md2FileConstants
 * @Description: md2File文件转换常量类
 * @author: skyeye云系列--卫志强
 * @date: 2021/6/14 11:56
 * @Copyright: 2021 https://gitee.com/doc_wei01/skyeye Inc. All rights reserved.
 * 注意：本内容仅限购买后使用.禁止私自外泄以及用于其他的商业目的
 */
public class Md2FileConstants {

    // 文件转换---图片类型
    public static final String[] SYS_FILE_CONSOLE_IS_IMAGES = { "png", "jpg", "xbm", "bmp", "webp", "jpeg", "svgz",
            "git", "ico", "tiff", "svg", "jiff", "pjpeg", "pjp", "tif" };

}
