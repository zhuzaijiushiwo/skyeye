/*******************************************************************************
 * Copyright 卫志强 QQ：598748873@qq.com Inc. All rights reserved. 开源地址：https://gitee.com/doc_wei01/skyeye
 ******************************************************************************/

package com.skyeye.common.constans;

/**
 * @ClassName: RedisConstants
 * @Description: redis常用工具类
 * @author: skyeye云系列--卫志强
 * @date: 2021/12/3 20:54
 * @Copyright: 2021 https://gitee.com/doc_wei01/skyeye Inc. All rights reserved.
 * 注意：本内容仅限购买后使用.禁止私自外泄以及用于其他的商业目的
 */
public class RedisConstants {

    /**
     * 默认key失效时间为十天
     */
    public static final int TEN_DAY_SECONDS = 10 * 24 * 60 * 60;

    /**
     * 默认key失效时间30天
     */
    public static final int THIRTY_DAY_SECONDS = 30 * 24 * 60 * 60;

}
