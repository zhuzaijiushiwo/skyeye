/*******************************************************************************
 * Copyright 卫志强 QQ：598748873@qq.com Inc. All rights reserved. 开源地址：https://gitee.com/doc_wei01/skyeye
 ******************************************************************************/

package com.skyeye.dao;

import java.util.List;
import java.util.Map;

public interface SealSeServiceCheckWorkDao {

	public List<Map<String, Object>> querySealSeServiceCheckWorkList(Map<String, Object> map) throws Exception;

}
